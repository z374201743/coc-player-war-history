package com.boot.utility;

import java.sql.Date;
import java.util.Calendar;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

@Component
public final class UtilityClass {

	@Value("${secrets.apiKey}")
	private static String apiKey;

	public static HttpEntity<String> generateCOCHeaderEntity() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("authorization", apiKey);
		headers.set("Accept", "application/json");
		HttpEntity<String> entity = new HttpEntity<>(headers);
		return entity;
	}

	public static Date getCurrentDateTimePlus48hrs() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 2);
		return new Date(cal.getTime().getTime());
	}

	public static Date getCurrentDateTime() {
		Calendar cal = Calendar.getInstance();
		return new Date(cal.getTime().getTime());
	}
}
