package com.boot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.boot.model.WarStat;

public interface WarStatsRepository extends JpaRepository<WarStat, Long> {

	final static String FIND_BY_CLAN_WAR_END_TIME = "SELECT P.CLANID, W.WAR_END_TIME "
			+ "FROM PLAYER AS P LEFT JOIN WARSTAT AS W ON P.PLAYERID = W.PLAYERID "
			+ "WHERE P.CLANID = ? AND W.WAR_END_TIME = ?";

	List<WarStat> findByPlayerID(String playerID);

	@Query(value = FIND_BY_CLAN_WAR_END_TIME, nativeQuery = true)
	List<WarStat> findByClanIDandWarEndTime(String clanID, String date);
}
