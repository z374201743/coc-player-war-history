package com.boot.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

	@RequestMapping("/")
	public String home(){
		return "COC War Stats /app/v1";
	}
	
	@RequestMapping("/test")
	public String test() {
		return "this is a test path";
	}
}
