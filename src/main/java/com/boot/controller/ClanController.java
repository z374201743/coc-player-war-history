package com.boot.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.boot.service.ClanService;

@RestController
@RequestMapping("api/v1/")
public class ClanController {

	@Autowired
	private ClanService clanService;

	@RequestMapping(value = "allClans", method = RequestMethod.GET)
	public Set<String> list() {
		return clanService.findAllClans();
	}

	@RequestMapping(value = "clan/{clanTag}", method = RequestMethod.POST)
	public Set<String> postClan(@PathVariable String clanTag) {
		return clanService.addClan(clanTag);
	}

}
