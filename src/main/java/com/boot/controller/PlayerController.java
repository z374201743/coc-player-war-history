package com.boot.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.boot.model.PlayerInfo;
import com.boot.service.PlayerService;

@RestController
@RequestMapping("api/v1/")
public class PlayerController {

	@Autowired
	private PlayerService playerService;
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@RequestMapping(value = "player/{playerID}", method = RequestMethod.GET)
	public PlayerInfo playerDetails(@PathVariable String playerID) {
		logger.info("in player controller for get method player");
		return playerService.getPlayer(playerID);
	}
	
	@RequestMapping(value = "allPlayers", method = RequestMethod.GET)
	public List<String> playerList() {
		logger.info("in player controller for get method player");
		return playerService.getPlayerList();
	}
}
