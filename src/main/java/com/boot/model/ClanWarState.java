package com.boot.model;

import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ClanWarState {
	private String state;
	private Date endTime;
	private WarClan clan;

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public WarClan getClan() {
		return clan;
	}

	public void setClan(WarClan clan) {
		this.clan = clan;
	}

}
