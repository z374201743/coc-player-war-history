package com.boot.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "warstat")
public class WarStat {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "playerid")
	private String playerID;

	@Column(name = "defender_tag")
	private String defenderTag;

	@Column(name = "stars")
	private int stars;

	@Column(name = "destruction_percentage")
	private int destructionPercentage;

	@Column(name = "war_end_time")
	private Date warEndTime;

	public String getDefenderTag() {
		return defenderTag;
	}

	public int getDestructionPercentage() {
		return destructionPercentage;
	}

	public int getId() {
		return id;
	}

	public String getPlayerID() {
		return playerID;
	}

	public int getStars() {
		return stars;
	}

	public Date getWarEndTime() {
		return warEndTime;
	}

	public void setDefenderTag(String defenderTag) {
		this.defenderTag = defenderTag;
	}

	public void setDestructionPercentage(int destructionPercentage) {
		this.destructionPercentage = destructionPercentage;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setPlayerID(String playerID) {
		this.playerID = playerID;
	}

	public void setStars(int stars) {
		this.stars = stars;
	}

	public void setWarEndTime(Date warEndTime) {
		this.warEndTime = warEndTime;
	}
}
