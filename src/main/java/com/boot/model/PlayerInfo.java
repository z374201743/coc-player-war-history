package com.boot.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PlayerInfo {
	private String tag;
	private String name;
	private short townHallLevel;
	private int bestTrophies;
	private int warStars;
	private Clan clan;
	private List<Troop> troops;
	private List<Hero> heroes;
	private List<Spell> spells;
	private List<WarStat> warStats;

	public int getBestTrophies() {
		return bestTrophies;
	}

	public Clan getClan() {
		return clan;
	}

	public List<Hero> getHeroes() {
		return heroes;
	}

	public String getName() {
		return name;
	}

	public List<Spell> getSpells() {
		return spells;
	}

	public String getTag() {
		return tag;
	}

	public short getTownHallLevel() {
		return townHallLevel;
	}

	public List<Troop> getTroops() {
		return troops;
	}

	public int getWarStars() {
		return warStars;
	}

	public List<WarStat> getWarStats() {
		return warStats;
	}

	public void setBestTrophies(int bestTrophies) {
		this.bestTrophies = bestTrophies;
	}

	public void setClan(Clan clan) {
		this.clan = clan;
	}

	public void setHeroes(List<Hero> heroes) {
		this.heroes = heroes;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSpells(List<Spell> spells) {
		this.spells = spells;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public void setTownHallLevel(short townHallLevel) {
		this.townHallLevel = townHallLevel;
	}

	public void setTroops(List<Troop> troops) {
		this.troops = troops;
	}

	public void setWarStars(int warStars) {
		this.warStars = warStars;
	}

	public void setWarStats(List<WarStat> warStats) {
		this.warStats = warStats;
	}

}
