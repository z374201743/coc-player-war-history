package com.boot.service;

import java.net.URI;
import java.sql.Date;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.boot.model.Attack;
import com.boot.model.ClanWarState;
import com.boot.model.Member;
import com.boot.model.WarStat;
import com.boot.repository.PlayerRepository;
import com.boot.repository.WarStatsRepository;
import com.boot.utility.UtilityClass;

@Service
public class WarStatsFetchService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	private static final String URL_PART1 = "https://api.clashofclans.com/v1/clans/%23";
	private static final String URL_PART2 = "/currentwar";
	private Map<String, Date> ignoredClansMap = new LinkedHashMap<>();
	private final RestTemplate restTemplate;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private WarStatsRepository warStatsRepository;

	public WarStatsFetchService(RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = restTemplateBuilder.build();
	}

	@Scheduled(cron = "*/5 * * * * *")
	public void cronJob() {
		//TODO: Refactor this method into smaller chunks
		logger.info("> cron task start");

		Set<String> clans = playerRepository.findAllClans();
		validateIgnoredClansList();

		clans = filterPersistanceList(clans);

		List<ClanWarState> clansToStore = new ArrayList<>();
		for (String clan : clans) {
			// TODO: handle 403 exceptions
			ClanWarState warState = getClanWarDetails(clan);
			// Check if clan war state is "warEnded", if yes add to persistence list
			if (ClanWarsStates.WarEnded.equalsState(warState.getState())) {
				clansToStore.add(warState);
			}
		}

		for (ClanWarState clanWar : clansToStore) {
			if (clanWar != null && clanWar.getClan() != null) {
				List<WarStat> warStatsList = getWarStats(clanWar.getClan().getTag(), clanWar.getEndTime());
				String clanTag = clanWar.getClan().getTag();
				if (warStatsList.size() == 0) {
					Date warEndTime = clanWar.getEndTime();
					List<Member> membersStats = clanWar.getClan().getMembers();
					for (Member membersStat : membersStats) {
						String playerId = membersStat.getTag();
						// There can only be a max of two attacks per member
						for (Attack attack : membersStat.getAttacks()) {
							WarStat warStat = new WarStat();
							warStat.setPlayerID(playerId);
							warStat.setDefenderTag(attack.getDefenderTag());
							warStat.setStars(attack.getStars());
							warStat.setDestructionPercentage(attack.getDestructionPercentage());
							warStat.setWarEndTime(warEndTime);
							warStatsRepository.saveAndFlush(warStat);
							ignoredClansMap.put(clanTag, UtilityClass.getCurrentDateTimePlus48hrs());
						}
					}
				} else {
					ignoredClansMap.put(clanTag, UtilityClass.getCurrentDateTimePlus48hrs());
				}
			}
		}

		logger.info(clans.toString());
		logger.info("< cron task end");
	}

	// Get clan war details from coc api
	private ClanWarState getClanWarDetails(String clan) {
		logger.info("> Making an coc current war api call");
		ClanWarState clanWarState = null;
		clan = clan.replace("#", clan);
		try {
			URI uri = new URI(URL_PART1 + clan + URL_PART2);
			logger.info(uri.toASCIIString());
			clanWarState = restTemplate
					.exchange(uri, HttpMethod.GET, UtilityClass.generateCOCHeaderEntity(), ClanWarState.class)
					.getBody();

		} catch (RestClientException ex) {
			logger.info("restful request failed with error");
			logger.error(ex.getMessage());
		} catch (Exception ex) {
			logger.error(ex.getMessage());
		}
		return clanWarState;
	}

	private Set<String> filterPersistanceList(Set<String> clansToStore) {
		Set<String> clansToBeIgnored = ignoredClansMap.keySet();
		clansToStore.removeAll(clansToBeIgnored);
		return clansToStore;
	}

	// Get war stats from database
	private List<WarStat> getWarStats(String clanTag, Date warEndTime) {
		return warStatsRepository.findByClanIDandWarEndTime(clanTag, warEndTime.toString());
	}

	private void validateIgnoredClansList() {
		Set<Entry<String, Date>> clanEntrySet = ignoredClansMap.entrySet();
		for (Entry<String, Date> clanEntry : clanEntrySet) {
			if (clanEntry.getValue().compareTo(UtilityClass.getCurrentDateTime()) > 0) {
				ignoredClansMap.remove(clanEntry.getKey());
			}
		}
	}
}
