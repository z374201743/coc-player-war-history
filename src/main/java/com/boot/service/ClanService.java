package com.boot.service;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.boot.repository.PlayerRepository;

@Service
public class ClanService {

	private static final String URL_PART1 = "https://api.clashofclans.com/v1/clans/%23";
	private static final String URL_PART2 = "/members";
	
	@Autowired
	private PlayerRepository playerRepository;

	public Set<String> findAllClans() {
		return playerRepository.findAllClans();
	}

	public Set<String> addClan(String clanTag) {
		// TODO: complete the fetch for member list from coc api
		// and store it to database
		return null;
	}

}
