package com.boot.service;

import java.net.URI;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.boot.model.PlayerInfo;
import com.boot.repository.PlayerRepository;
import com.boot.repository.WarStatsRepository;
import com.boot.utility.UtilityClass;

@Service
public class PlayerService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	private static final String URL = "https://api.clashofclans.com/v1/players/%23";
	private final RestTemplate restTemplate;

	@Autowired
	private WarStatsRepository warStatsRepository;

	@Autowired
	private PlayerRepository playerRepository;

	public PlayerService(RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = restTemplateBuilder.build();
	}

	public PlayerInfo getPlayer(String playerID) {
		PlayerInfo player = null;
		playerID = playerID.replace("#", "");
		logger.info("> Making an coc players api call");
		try {
			URI uri = new URI(URL + playerID);
			logger.info(uri.toASCIIString());
			player = restTemplate.exchange(uri, HttpMethod.GET, UtilityClass.generateCOCHeaderEntity(), PlayerInfo.class).getBody();
			logger.info("successful player api call");
		} catch (RestClientException ex) {
			logger.info("restful request failed with error");
			logger.error(ex.getMessage());
		} catch (Exception ex) {
			logger.error(ex.getMessage());
		}
		logger.info("< coc player api call ended");
		player.setTag(playerID);
		player.setWarStats(warStatsRepository.findByPlayerID(playerID));
		return player;
	}

	public List<String> getPlayerList() {
		return playerRepository.findAllPlayers();
	}
}
