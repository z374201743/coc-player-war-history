package com.boot.service;

import java.net.URI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.boot.model.Player;

@Service
public class ClashOfClansService {

	private final String token = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImtpZCI6IjI4YTMxOGY3LTAwMDAtYTFlYi03ZmExLTJjNzQzM2M2Y2NhNSJ9.eyJpc3MiOiJzdXBlcmNlbGwiLCJhdWQiOiJzdXBlcmNlbGw6Z2FtZWFwaSIsImp0aSI6ImIxODI3NGYyLTE3MDMtNDEzYi04YzI3LWNhMTliOTFlNmY3MyIsImlhdCI6MTU0MTk2MTM2Niwic3ViIjoiZGV2ZWxvcGVyL2RkNmFiMTY4LWFhZTQtMDk0NS1kNjM0LWJmNjhkM2FlNTkyNSIsInNjb3BlcyI6WyJjbGFzaCJdLCJsaW1pdHMiOlt7InRpZXIiOiJkZXZlbG9wZXIvc2lsdmVyIiwidHlwZSI6InRocm90dGxpbmcifSx7ImNpZHJzIjpbIjI0LjEzOC41NS4xMDIiXSwidHlwZSI6ImNsaWVudCJ9XX0.dqBF21pXJyLq1k4kFi33m8cq1IOFDcv4IFeHO7jJJal_8kBC3b8WVmkCdFjepH0tiXG722gRZ2VRz1WRQSX7UQ";
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	private final RestTemplate restTemplate;

	public ClashOfClansService(RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = restTemplateBuilder.build();
	}

	public Player getPlayer(String playerID) {
		logger.info("> started on clash of clans");
		String url = null;
		HttpHeaders headers = new HttpHeaders();
		headers.set("authorization", this.token);
		headers.set("Accept", "application/json");
		HttpEntity<?> entity = new HttpEntity<>(headers);
		Player player = null;
		url = "https://api.clashofclans.com/v1/players/%23RGQLRQ9R";
		try {
			logger.info(url);
			URI uri = new URI(url);
			player = restTemplate.exchange(uri, HttpMethod.GET, entity, Player.class).getBody();
		} catch (Exception e) {
			logger.info(e.toString());
		}
//		logger.info(player.getName());
		if (player != null) {
			logger.info("this is the player info being printed" + player);
			logger.info("*************************************");
		}

		return player;
	}

}
